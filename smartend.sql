-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-08-2017 a las 00:27:30
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `smartend`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_analytics_pages`
--

CREATE TABLE `smartend_analytics_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `visitor_id` int(11) NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `query` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `load_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `smartend_analytics_pages`
--

INSERT INTO `smartend_analytics_pages` (`id`, `visitor_id`, `ip`, `title`, `name`, `query`, `load_time`, `date`, `time`, `created_at`, `updated_at`) VALUES
(1, 1, '127.0.0.1', 'http://localhost:8000/admin/1/topics', 'unknown', 'http://localhost:8000/admin/1/topics', '0.25101495', '2017-08-28', '21:57:22', '2017-08-29 00:57:22', '2017-08-29 00:57:22'),
(2, 1, '127.0.0.1', 'http://localhost:8000/admin?_pjax=%23view', 'unknown', 'http://localhost:8000/admin?_pjax=%23view', '0.28901696', '2017-08-28', '21:57:25', '2017-08-29 00:57:25', '2017-08-29 00:57:25'),
(3, 1, '127.0.0.1', 'http://localhost:8000/admin', 'unknown', 'http://localhost:8000/admin', '0.36101985', '2017-08-28', '21:57:26', '2017-08-29 00:57:26', '2017-08-29 00:57:26'),
(4, 1, '127.0.0.1', 'http://localhost:8000/admin/2/topics?_pjax=%23view', 'unknown', 'http://localhost:8000/admin/2/topics?_pjax=%23view', '0.23801398', '2017-08-28', '21:57:38', '2017-08-29 00:57:38', '2017-08-29 00:57:38'),
(5, 1, '127.0.0.1', 'http://localhost:8000/admin/4/topics?_pjax=%23view', 'unknown', 'http://localhost:8000/admin/4/topics?_pjax=%23view', '0.216012', '2017-08-28', '21:57:40', '2017-08-29 00:57:40', '2017-08-29 00:57:40'),
(6, 1, '127.0.0.1', 'http://localhost:8000/admin/3/topics?_pjax=%23view', 'unknown', 'http://localhost:8000/admin/3/topics?_pjax=%23view', '0.26001501', '2017-08-28', '21:57:40', '2017-08-29 00:57:40', '2017-08-29 00:57:40'),
(7, 1, '127.0.0.1', 'http://localhost:8000/admin/webmaster?_pjax=%23view', 'unknown', 'http://localhost:8000/admin/webmaster?_pjax=%23view', '0.29101706', '2017-08-28', '21:57:49', '2017-08-29 00:57:49', '2017-08-29 00:57:49'),
(8, 1, '127.0.0.1', 'http://localhost:8000/admin/webmaster/banners?_pjax=%23view', 'unknown', 'http://localhost:8000/admin/webmaster/banners?_pjax=%23view', '0.24601388', '2017-08-28', '21:58:02', '2017-08-29 00:58:02', '2017-08-29 00:58:02'),
(9, 1, '127.0.0.1', 'http://localhost:8000/admin/webmaster/sections?_pjax=%23view', 'unknown', 'http://localhost:8000/admin/webmaster/sections?_pjax=%23view', '0.25401402', '2017-08-28', '21:58:04', '2017-08-29 00:58:04', '2017-08-29 00:58:04'),
(10, 1, '127.0.0.1', 'Titulo del sitio', 'unknown', 'http://localhost:8000/', '0.36002016', '2017-08-28', '21:58:08', '2017-08-29 00:58:08', '2017-08-29 00:58:08'),
(11, 1, '127.0.0.1', 'http://localhost:8000/admin/settings', 'unknown', 'http://localhost:8000/admin/settings', '0.33501911', '2017-08-28', '21:59:31', '2017-08-29 00:59:31', '2017-08-29 00:59:31'),
(12, 1, '127.0.0.1', 'http://localhost:8000/admin/settings?_pjax=%23view', 'unknown', 'http://localhost:8000/admin/settings?_pjax=%23view', '0.3700211', '2017-08-28', '21:59:31', '2017-08-29 00:59:31', '2017-08-29 00:59:31'),
(13, 1, '127.0.0.1', 'http://localhost:8000/admin/menus?_pjax=%23view', 'unknown', 'http://localhost:8000/admin/menus?_pjax=%23view', '0.31501794', '2017-08-28', '22:00:24', '2017-08-29 01:00:24', '2017-08-29 01:00:24'),
(14, 1, '127.0.0.1', 'http://localhost:8000/admin/menus/9/edit/1', 'unknown', 'http://localhost:8000/admin/menus/9/edit/1', '0.23201299', '2017-08-28', '22:00:33', '2017-08-29 01:00:33', '2017-08-29 01:00:33'),
(15, 1, '127.0.0.1', 'http://localhost:8000/admin/menus/1?id=9', 'unknown', 'http://localhost:8000/admin/menus/1?id=9', '0.25401497', '2017-08-28', '22:00:38', '2017-08-29 01:00:38', '2017-08-29 01:00:38'),
(16, 1, '127.0.0.1', 'Titulo del sitio', 'unknown', 'http://localhost:8000/topic/about', '0.60703492', '2017-08-28', '22:00:48', '2017-08-29 01:00:48', '2017-08-29 01:00:48'),
(17, 1, '127.0.0.1', 'http://localhost:8000/admin/menus/4/edit/1', 'unknown', 'http://localhost:8000/admin/menus/4/edit/1', '0.21401286', '2017-08-28', '22:01:03', '2017-08-29 01:01:03', '2017-08-29 01:01:03'),
(18, 1, '127.0.0.1', 'http://localhost:8000/admin/1/topics?_pjax=%23view', 'unknown', 'http://localhost:8000/admin/1/topics?_pjax=%23view', '0.31401801', '2017-08-28', '22:01:11', '2017-08-29 01:01:11', '2017-08-29 01:01:11'),
(19, 1, '127.0.0.1', 'http://localhost:8000/admin/1/topics/1/edit', 'unknown', 'http://localhost:8000/admin/1/topics/1/edit', '0.36002016', '2017-08-28', '22:01:13', '2017-08-29 01:01:13', '2017-08-29 01:01:13'),
(20, 1, '127.0.0.1', 'http://localhost:8000/admin/webmaster', 'unknown', 'http://localhost:8000/admin/webmaster', '0.24001408', '2017-08-28', '22:01:42', '2017-08-29 01:01:42', '2017-08-29 01:01:42'),
(21, 1, '127.0.0.1', 'http://localhost:8000/admin/2/topics', 'unknown', 'http://localhost:8000/admin/2/topics', '0.216012', '2017-08-28', '22:11:47', '2017-08-29 01:11:47', '2017-08-29 01:11:47'),
(22, 1, '127.0.0.1', 'http://localhost:8000/admin/2/topics/create', 'unknown', 'http://localhost:8000/admin/2/topics/create', '0.22501302', '2017-08-28', '22:14:54', '2017-08-29 01:14:54', '2017-08-29 01:14:54'),
(23, 1, '127.0.0.1', 'http://localhost:8000/admin/2/topics/5/edit', 'unknown', 'http://localhost:8000/admin/2/topics/5/edit', '0.28201604', '2017-08-28', '22:15:16', '2017-08-29 01:15:16', '2017-08-29 01:15:16'),
(24, 1, '127.0.0.1', 'http://localhost:8000/admin/2/topics/6/edit', 'unknown', 'http://localhost:8000/admin/2/topics/6/edit', '0.26401496', '2017-08-28', '22:15:35', '2017-08-29 01:15:35', '2017-08-29 01:15:35'),
(25, 1, '127.0.0.1', 'Servicio 1', 'unknown', 'http://localhost:8000/services/topic/5', '0.54903102', '2017-08-28', '22:15:43', '2017-08-29 01:15:43', '2017-08-29 01:15:43'),
(26, 1, '127.0.0.1', 'Contáctenos', 'unknown', 'http://localhost:8000/contact', '0.51702905', '2017-08-28', '22:16:01', '2017-08-29 01:16:01', '2017-08-29 01:16:01'),
(27, 1, '127.0.0.1', 'http://localhost:8000/admin/8/sections?_pjax=%23view', 'unknown', 'http://localhost:8000/admin/8/sections?_pjax=%23view', '0.42502522', '2017-08-28', '22:17:00', '2017-08-29 01:17:00', '2017-08-29 01:17:00'),
(28, 1, '127.0.0.1', 'http://localhost:8000/admin/8/sections/create', 'unknown', 'http://localhost:8000/admin/8/sections/create', '0.22401285', '2017-08-28', '22:17:02', '2017-08-29 01:17:02', '2017-08-29 01:17:02'),
(29, 1, '127.0.0.1', 'http://localhost:8000/admin/8/sections', 'unknown', 'http://localhost:8000/admin/8/sections', '0.27701592', '2017-08-28', '22:17:21', '2017-08-29 01:17:21', '2017-08-29 01:17:21'),
(30, 1, '127.0.0.1', 'http://localhost:8000/admin/8/sections/1/edit', 'unknown', 'http://localhost:8000/admin/8/sections/1/edit', '0.26801491', '2017-08-28', '22:17:24', '2017-08-29 01:17:24', '2017-08-29 01:17:24'),
(31, 1, '127.0.0.1', 'http://localhost:8000/admin/8/topics?_pjax=%23view', 'unknown', 'http://localhost:8000/admin/8/topics?_pjax=%23view', '0.21501184', '2017-08-28', '22:17:42', '2017-08-29 01:17:42', '2017-08-29 01:17:42'),
(32, 1, '127.0.0.1', 'http://localhost:8000/admin/8/topics/create', 'unknown', 'http://localhost:8000/admin/8/topics/create', '0.222013', '2017-08-28', '22:17:48', '2017-08-29 01:17:48', '2017-08-29 01:17:48'),
(33, 1, '127.0.0.1', 'http://localhost:8000/admin/8/topics/7/edit', 'unknown', 'http://localhost:8000/admin/8/topics/7/edit', '0.39602184', '2017-08-28', '22:18:08', '2017-08-29 01:18:08', '2017-08-29 01:18:08'),
(34, 1, '127.0.0.1', 'Categoría 1', 'unknown', 'http://localhost:8000/products/1', '0.45002508', '2017-08-28', '22:18:18', '2017-08-29 01:18:18', '2017-08-29 01:18:18'),
(35, 1, '127.0.0.1', 'Producto 1', 'unknown', 'http://localhost:8000/products/topic/7', '0.37302113', '2017-08-28', '22:18:29', '2017-08-29 01:18:29', '2017-08-29 01:18:29'),
(36, 1, '127.0.0.1', 'Noticia', 'unknown', 'http://localhost:8000/news', '0.35301995', '2017-08-28', '22:18:56', '2017-08-29 01:18:56', '2017-08-29 01:18:56'),
(37, 1, '127.0.0.1', 'Imagen', 'unknown', 'http://localhost:8000/photos', '0.3820219', '2017-08-28', '22:19:00', '2017-08-29 01:19:00', '2017-08-29 01:19:00'),
(38, 1, '127.0.0.1', 'Titulo del sitio', 'unknown', 'http://localhost:8000/home', '0.403023', '2017-08-28', '22:19:06', '2017-08-29 01:19:06', '2017-08-29 01:19:06'),
(39, 1, '127.0.0.1', 'http://localhost:8000/admin/banners?_pjax=%23view', 'unknown', 'http://localhost:8000/admin/banners?_pjax=%23view', '0.26101494', '2017-08-28', '22:22:56', '2017-08-29 01:22:56', '2017-08-29 01:22:56'),
(40, 1, '127.0.0.1', 'http://localhost:8000/admin/banners/1/edit', 'unknown', 'http://localhost:8000/admin/banners/1/edit', '0.24401379', '2017-08-28', '22:22:58', '2017-08-29 01:22:58', '2017-08-29 01:22:58'),
(41, 1, '127.0.0.1', 'http://localhost:8000/uploads/banners/noimg.png', 'unknown', 'http://localhost:8000/uploads/banners/noimg.png', '0.34001899', '2017-08-28', '22:23:21', '2017-08-29 01:23:21', '2017-08-29 01:23:21'),
(42, 1, '127.0.0.1', 'http://localhost:8000/admin/banners/2/edit', 'unknown', 'http://localhost:8000/admin/banners/2/edit', '0.23001289', '2017-08-28', '22:23:43', '2017-08-29 01:23:43', '2017-08-29 01:23:43'),
(43, 1, '127.0.0.1', 'http://localhost:8000/admin/banners/create/1', 'unknown', 'http://localhost:8000/admin/banners/create/1', '0.25301409', '2017-08-28', '22:24:19', '2017-08-29 01:24:19', '2017-08-29 01:24:19'),
(44, 1, '127.0.0.1', 'http://localhost:8000/admin/banners', 'unknown', 'http://localhost:8000/admin/banners', '0.24201417', '2017-08-28', '22:24:38', '2017-08-29 01:24:38', '2017-08-29 01:24:38'),
(45, 1, '127.0.0.1', 'Servicio 2', 'unknown', 'http://localhost:8000/services/topic/6', '0.42402387', '2017-08-28', '22:26:31', '2017-08-29 01:26:31', '2017-08-29 01:26:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_analytics_visitors`
--

CREATE TABLE `smartend_analytics_visitors` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_cor1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_cor2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `os` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `browser` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resolution` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referrer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hostname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `org` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `smartend_analytics_visitors`
--

INSERT INTO `smartend_analytics_visitors` (`id`, `ip`, `city`, `country_code`, `country`, `region`, `full_address`, `location_cor1`, `location_cor2`, `os`, `browser`, `resolution`, `referrer`, `hostname`, `org`, `date`, `time`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 7', 'Chrome', 'unknown', 'http://localhost:8000/admin/3/topics', 'No Hostname', 'unknown', '2017-08-28', '21:57:22', '2017-08-29 00:57:22', '2017-08-29 00:57:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_banners`
--

CREATE TABLE `smartend_banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `section_id` int(11) NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_es` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_ar` text COLLATE utf8mb4_unicode_ci,
  `details_en` text COLLATE utf8mb4_unicode_ci,
  `details_es` text COLLATE utf8mb4_unicode_ci,
  `code` text COLLATE utf8mb4_unicode_ci,
  `file_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_es` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_type` tinyint(4) DEFAULT NULL,
  `youtube_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `visits` int(11) NOT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `smartend_banners`
--

INSERT INTO `smartend_banners` (`id`, `section_id`, `title_ar`, `title_en`, `title_es`, `details_ar`, `details_en`, `details_es`, `code`, `file_ar`, `file_en`, `file_es`, `video_type`, `youtube_link`, `link_url`, `icon`, `status`, `visits`, `row_no`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'بنر رقم ١', NULL, 'Banner #1', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', NULL, 'Es un hecho establecido desde hace mucho tiempo que un lector se distraiga por el contenido legible de una página', NULL, 'noimg.png', '15039589948144.jpg', '15039589946147.jpg', NULL, NULL, '#', NULL, 1, 0, 1, 1, 1, '2017-08-29 00:57:10', '2017-08-29 01:23:14'),
(2, 1, 'بنر رقم ٢', NULL, 'Banner #2', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', NULL, 'Es un hecho establecido desde hace mucho tiempo que un lector se distraiga por el contenido legible de una página', NULL, 'noimg.png', '15039590427125.jpg', '15039590428989.jpg', NULL, NULL, '#', NULL, 1, 0, 2, 1, 1, '2017-08-29 00:57:10', '2017-08-29 01:24:02'),
(3, 2, 'تصميم ريسبونسف', 'Responsive Design', 'Responsive Design', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', 'Es un hecho establecido desde hace mucho tiempo que un lector se distraiga por el contenido legible de una página', NULL, '', '', '', NULL, NULL, '#', 'fa-object-group', 1, 0, 1, 1, NULL, '2017-08-29 00:57:10', '2017-08-29 00:57:10'),
(4, 2, ' احدث التقنيات', 'HTML5 & CSS3', 'HTML5 & CSS3', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', 'Es un hecho establecido desde hace mucho tiempo que un lector se distraiga por el contenido legible de una página', NULL, '', '', '', NULL, NULL, '#', 'fa-html5', 1, 0, 2, 1, NULL, '2017-08-29 00:57:10', '2017-08-29 00:57:10'),
(5, 2, 'باستخدام بوتستراب', 'Bootstrap Used', 'Utilización de Bootstrap', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', 'Es un hecho establecido desde hace mucho tiempo que un lector se distraiga por el contenido legible de una página', NULL, '', '', '', NULL, NULL, '#', 'fa-code', 1, 0, 3, 1, NULL, '2017-08-29 00:57:10', '2017-08-29 00:57:10'),
(6, 2, 'تصميم كلاسيكي', 'Classic Design', 'Diseño Clasico', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', 'Es un hecho establecido desde hace mucho tiempo que un lector se distraiga por el contenido legible de una página', NULL, '', '', '', NULL, NULL, '#', 'fa-laptop', 1, 0, 4, 1, NULL, '2017-08-29 00:57:10', '2017-08-29 00:57:10'),
(7, 1, NULL, NULL, 'Banner #3', NULL, NULL, 'Es un hecho establecido desde hace mucho tiempo que un lector se distraiga por el contenido legible de una página', NULL, NULL, '15039590772144.jpg', '15039590778268.jpg', NULL, NULL, NULL, NULL, 1, 0, 5, 1, NULL, '2017-08-29 01:24:37', '2017-08-29 01:24:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_comments`
--

CREATE TABLE `smartend_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_contacts`
--

CREATE TABLE `smartend_contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `last_login` datetime DEFAULT NULL,
  `last_login_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_contacts_groups`
--

CREATE TABLE `smartend_contacts_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `smartend_contacts_groups`
--

INSERT INTO `smartend_contacts_groups` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Suscriptores', 1, NULL, '2017-08-29 00:57:09', '2017-08-29 00:57:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_countries`
--

CREATE TABLE `smartend_countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_es` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `smartend_countries`
--

INSERT INTO `smartend_countries` (`id`, `code`, `title_ar`, `title_en`, `title_es`, `tel`, `created_at`, `updated_at`) VALUES
(1, 'AL', 'ألبانيا', 'Albania', NULL, '355', '2017-08-29 00:56:57', '2017-08-29 00:56:57'),
(2, 'DZ', 'الجزائر', 'Algeria', NULL, '213', '2017-08-29 00:56:57', '2017-08-29 00:56:57'),
(3, 'AS', 'ساموا الأمريكية', 'American Samoa', NULL, '1-684', '2017-08-29 00:56:57', '2017-08-29 00:56:57'),
(4, 'AD', 'أندورا', 'Andorra', NULL, '376', '2017-08-29 00:56:57', '2017-08-29 00:56:57'),
(5, 'AO', 'أنغولا', 'Angola', NULL, '244', '2017-08-29 00:56:57', '2017-08-29 00:56:57'),
(6, 'AI', 'أنغيلا', 'Anguilla', NULL, '1-264', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(7, 'AR', 'الأرجنتين', 'Argentina', NULL, '54', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(8, 'AM', 'أرمينيا', 'Armenia', NULL, '374', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(9, 'AW', 'أروبا', 'Aruba', NULL, '297', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(10, 'AU', 'أستراليا', 'Australia', NULL, '61', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(11, 'AT', 'النمسا', 'Austria', NULL, '43', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(12, 'AZ', 'أذربيجان', 'Azerbaijan', NULL, '994', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(13, 'BS', 'جزر البهاما', 'Bahamas', NULL, '1-242', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(14, 'BH', 'البحرين', 'Bahrain', NULL, '973', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(15, 'BD', 'بنغلاديش', 'Bangladesh', NULL, '880', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(16, 'BB', 'بربادوس', 'Barbados', NULL, '1-246', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(17, 'BY', 'روسيا البيضاء', 'Belarus', NULL, '375', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(18, 'BE', 'بلجيكا', 'Belgium', NULL, '32', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(19, 'BZ', 'بليز', 'Belize', NULL, '501', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(20, 'BJ', 'بنين', 'Benin', NULL, '229', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(21, 'BM', 'برمودا', 'Bermuda', NULL, '1-441', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(22, 'BT', 'بوتان', 'Bhutan', NULL, '975', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(23, 'BO', 'بوليفيا', 'Bolivia', 'Bolivia', '591', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(24, 'BA', 'البوسنة والهرسك', 'Bosnia and Herzegovina', NULL, '387', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(25, 'BW', 'بوتسوانا', 'Botswana', NULL, '267', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(26, 'BR', 'البرازيل', 'Brazil', 'Brasil', '55', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(27, 'VG', 'جزر فيرجن البريطانية', 'British Virgin Islands', NULL, '1-284', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(28, 'IO', 'إقليم المحيط الهندي البريطاني', 'British Indian Ocean Territory', NULL, '246', '2017-08-29 00:56:58', '2017-08-29 00:56:58'),
(29, 'BN', 'بروناي دار السلام', 'Brunei Darussalam', NULL, '673', '2017-08-29 00:56:59', '2017-08-29 00:56:59'),
(30, 'BG', 'بلغاريا', 'Bulgaria', NULL, '359', '2017-08-29 00:56:59', '2017-08-29 00:56:59'),
(31, 'BF', 'بوركينا فاسو', 'Burkina Faso', NULL, '226', '2017-08-29 00:56:59', '2017-08-29 00:56:59'),
(32, 'BI', 'بوروندي', 'Burundi', NULL, '257', '2017-08-29 00:56:59', '2017-08-29 00:56:59'),
(33, 'KH', 'كمبوديا', 'Cambodia', NULL, '855', '2017-08-29 00:56:59', '2017-08-29 00:56:59'),
(34, 'CM', 'الكاميرون', 'Cameroon', NULL, '237', '2017-08-29 00:56:59', '2017-08-29 00:56:59'),
(35, 'CA', 'كندا', 'Canada', NULL, '1', '2017-08-29 00:56:59', '2017-08-29 00:56:59'),
(36, 'CV', 'الرأس الأخضر', 'Cape Verde', NULL, '238', '2017-08-29 00:56:59', '2017-08-29 00:56:59'),
(37, 'KY', 'جزر كايمان', 'Cayman Islands', NULL, '1-345', '2017-08-29 00:56:59', '2017-08-29 00:56:59'),
(38, 'CF', 'افريقيا الوسطى', 'Central African Republic', NULL, '236', '2017-08-29 00:56:59', '2017-08-29 00:56:59'),
(39, 'TD', 'تشاد', 'Chad', NULL, '235', '2017-08-29 00:56:59', '2017-08-29 00:56:59'),
(40, 'CL', 'تشيلي', 'Chile', 'Chile', '56', '2017-08-29 00:56:59', '2017-08-29 00:56:59'),
(41, 'CN', 'الصين', 'China', NULL, '86', '2017-08-29 00:56:59', '2017-08-29 00:56:59'),
(42, 'HK', 'هونغ كونغ', 'Hong Kong', NULL, '852', '2017-08-29 00:56:59', '2017-08-29 00:56:59'),
(43, 'MO', 'ماكاو', 'Macao', NULL, '853', '2017-08-29 00:57:00', '2017-08-29 00:57:00'),
(44, 'CX', 'جزيرة الكريسماس', 'Christmas Island', NULL, '61', '2017-08-29 00:57:00', '2017-08-29 00:57:00'),
(45, 'CC', 'جزر كوكوس (كيلينغ)', 'Cocos (Keeling) Islands', NULL, '61', '2017-08-29 00:57:00', '2017-08-29 00:57:00'),
(46, 'CO', 'كولومبيا', 'Colombia', NULL, '57', '2017-08-29 00:57:00', '2017-08-29 00:57:00'),
(47, 'KM', 'جزر القمر', 'Comoros', NULL, '269', '2017-08-29 00:57:00', '2017-08-29 00:57:00'),
(48, 'CK', 'جزر كوك', 'Cook Islands', NULL, '682', '2017-08-29 00:57:00', '2017-08-29 00:57:00'),
(49, 'CR', 'كوستا ريكا', 'Costa Rica', NULL, '506', '2017-08-29 00:57:00', '2017-08-29 00:57:00'),
(50, 'HR', 'كرواتيا', 'Croatia', NULL, '385', '2017-08-29 00:57:00', '2017-08-29 00:57:00'),
(51, 'CU', 'كوبا', 'Cuba', NULL, '53', '2017-08-29 00:57:00', '2017-08-29 00:57:00'),
(52, 'CY', 'قبرص', 'Cyprus', NULL, '357', '2017-08-29 00:57:00', '2017-08-29 00:57:00'),
(53, 'CZ', 'الجمهورية التشيكية', 'Czech Republic', NULL, '420', '2017-08-29 00:57:00', '2017-08-29 00:57:00'),
(54, 'DK', 'الدنمارك', 'Denmark', NULL, '45', '2017-08-29 00:57:00', '2017-08-29 00:57:00'),
(55, 'DJ', 'جيبوتي', 'Djibouti', NULL, '253', '2017-08-29 00:57:00', '2017-08-29 00:57:00'),
(56, 'DM', 'دومينيكا', 'Dominica', NULL, '1-767', '2017-08-29 00:57:00', '2017-08-29 00:57:00'),
(57, 'DO', 'جمهورية الدومينيكان', 'Dominican Republic', NULL, '1-809', '2017-08-29 00:57:00', '2017-08-29 00:57:00'),
(58, 'EC', 'الاكوادور', 'Ecuador', NULL, '593', '2017-08-29 00:57:00', '2017-08-29 00:57:00'),
(59, 'EG', 'مصر', 'Egypt', NULL, '20', '2017-08-29 00:57:01', '2017-08-29 00:57:01'),
(60, 'SV', 'السلفادور', 'El Salvador', NULL, '503', '2017-08-29 00:57:01', '2017-08-29 00:57:01'),
(61, 'GQ', 'غينيا الاستوائية', 'Equatorial Guinea', NULL, '240', '2017-08-29 00:57:01', '2017-08-29 00:57:01'),
(62, 'ER', 'إريتريا', 'Eritrea', NULL, '291', '2017-08-29 00:57:01', '2017-08-29 00:57:01'),
(63, 'EE', 'استونيا', 'Estonia', NULL, '372', '2017-08-29 00:57:01', '2017-08-29 00:57:01'),
(64, 'ET', 'أثيوبيا', 'Ethiopia', NULL, '251', '2017-08-29 00:57:01', '2017-08-29 00:57:01'),
(65, 'FO', 'جزر فارو', 'Faroe Islands', NULL, '298', '2017-08-29 00:57:01', '2017-08-29 00:57:01'),
(66, 'FJ', 'فيجي', 'Fiji', NULL, '679', '2017-08-29 00:57:01', '2017-08-29 00:57:01'),
(67, 'FI', 'فنلندا', 'Finland', NULL, '358', '2017-08-29 00:57:01', '2017-08-29 00:57:01'),
(68, 'FR', 'فرنسا', 'France', NULL, '33', '2017-08-29 00:57:01', '2017-08-29 00:57:01'),
(69, 'GF', 'جيانا الفرنسية', 'French Guiana', NULL, '689', '2017-08-29 00:57:01', '2017-08-29 00:57:01'),
(70, 'GA', 'الغابون', 'Gabon', NULL, '241', '2017-08-29 00:57:01', '2017-08-29 00:57:01'),
(71, 'GM', 'غامبيا', 'Gambia', NULL, '220', '2017-08-29 00:57:01', '2017-08-29 00:57:01'),
(72, 'GE', 'جورجيا', 'Georgia', NULL, '995', '2017-08-29 00:57:01', '2017-08-29 00:57:01'),
(73, 'DE', 'ألمانيا', 'Germany', NULL, '49', '2017-08-29 00:57:01', '2017-08-29 00:57:01'),
(74, 'GH', 'غانا', 'Ghana', NULL, '233', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(75, 'GI', 'جبل طارق', 'Gibraltar', NULL, '350', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(76, 'GR', 'يونان', 'Greece', NULL, '30', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(77, 'GL', 'غرينلاند', 'Greenland', NULL, '299', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(78, 'GD', 'غرينادا', 'Grenada', NULL, '1-473', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(79, 'GU', 'غوام', 'Guam', NULL, '1-671', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(80, 'GT', 'غواتيمالا', 'Guatemala', NULL, '502', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(81, 'GN', 'غينيا', 'Guinea', NULL, '224', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(82, 'GW', 'غينيا-بيساو', 'Guinea-Bissau', NULL, '245', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(83, 'GY', 'غيانا', 'Guyana', NULL, '592', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(84, 'HT', 'هايتي', 'Haiti', NULL, '509', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(85, 'HN', 'هندوراس', 'Honduras', NULL, '504', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(86, 'HU', 'هنغاريا', 'Hungary', NULL, '36', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(87, 'IS', 'أيسلندا', 'Iceland', NULL, '354', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(88, 'IN', 'الهند', 'India', NULL, '91', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(89, 'ID', 'أندونيسيا', 'Indonesia', NULL, '62', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(90, 'IR', 'جمهورية إيران الإسلامية', 'Iran, Islamic Republic of', NULL, '98', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(91, 'IQ', 'العراق', 'Iraq', NULL, '964', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(92, 'IE', 'أيرلندا', 'Ireland', NULL, '353', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(93, 'IM', 'جزيرة مان', 'Isle of Man', NULL, '44-1624', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(94, 'IL', 'إسرائيل', 'Israel', NULL, '972', '2017-08-29 00:57:02', '2017-08-29 00:57:02'),
(95, 'IT', 'إيطاليا', 'Italy', NULL, '39', '2017-08-29 00:57:03', '2017-08-29 00:57:03'),
(96, 'JM', 'جامايكا', 'Jamaica', NULL, '1-876', '2017-08-29 00:57:03', '2017-08-29 00:57:03'),
(97, 'JP', 'اليابان', 'Japan', NULL, '81', '2017-08-29 00:57:03', '2017-08-29 00:57:03'),
(98, 'JE', 'جيرسي', 'Jersey', NULL, '44-1534', '2017-08-29 00:57:03', '2017-08-29 00:57:03'),
(99, 'JO', 'الأردن', 'Jordan', NULL, '962', '2017-08-29 00:57:03', '2017-08-29 00:57:03'),
(100, 'KZ', 'كازاخستان', 'Kazakhstan', NULL, '7', '2017-08-29 00:57:03', '2017-08-29 00:57:03'),
(101, 'KE', 'كينيا', 'Kenya', NULL, '254', '2017-08-29 00:57:03', '2017-08-29 00:57:03'),
(102, 'KI', 'كيريباس', 'Kiribati', NULL, '686', '2017-08-29 00:57:03', '2017-08-29 00:57:03'),
(103, 'KW', 'الكويت', 'Kuwait', NULL, '965', '2017-08-29 00:57:03', '2017-08-29 00:57:03'),
(104, 'KG', 'قيرغيزستان', 'Kyrgyzstan', NULL, '996', '2017-08-29 00:57:03', '2017-08-29 00:57:03'),
(105, 'LV', 'لاتفيا', 'Latvia', NULL, '371', '2017-08-29 00:57:03', '2017-08-29 00:57:03'),
(106, 'LB', 'لبنان', 'Lebanon', NULL, '961', '2017-08-29 00:57:03', '2017-08-29 00:57:03'),
(107, 'LS', 'ليسوتو', 'Lesotho', NULL, '266', '2017-08-29 00:57:03', '2017-08-29 00:57:03'),
(108, 'LR', 'ليبيريا', 'Liberia', NULL, '231', '2017-08-29 00:57:03', '2017-08-29 00:57:03'),
(109, 'LY', 'ليبيا', 'Libya', NULL, '218', '2017-08-29 00:57:03', '2017-08-29 00:57:03'),
(110, 'LI', 'ليشتنشتاين', 'Liechtenstein', NULL, '423', '2017-08-29 00:57:03', '2017-08-29 00:57:03'),
(111, 'LT', 'ليتوانيا', 'Lithuania', NULL, '370', '2017-08-29 00:57:03', '2017-08-29 00:57:03'),
(112, 'LU', 'لوكسمبورغ', 'Luxembourg', NULL, '352', '2017-08-29 00:57:03', '2017-08-29 00:57:03'),
(113, 'MK', 'مقدونيا، جمهورية', 'Macedonia', NULL, '389', '2017-08-29 00:57:04', '2017-08-29 00:57:04'),
(114, 'MG', 'مدغشقر', 'Madagascar', NULL, '261', '2017-08-29 00:57:04', '2017-08-29 00:57:04'),
(115, 'MW', 'ملاوي', 'Malawi', NULL, '265', '2017-08-29 00:57:04', '2017-08-29 00:57:04'),
(116, 'MY', 'ماليزيا', 'Malaysia', NULL, '60', '2017-08-29 00:57:04', '2017-08-29 00:57:04'),
(117, 'MV', 'جزر المالديف', 'Maldives', NULL, '960', '2017-08-29 00:57:04', '2017-08-29 00:57:04'),
(118, 'ML', 'مالي', 'Mali', NULL, '223', '2017-08-29 00:57:04', '2017-08-29 00:57:04'),
(119, 'MT', 'مالطا', 'Malta', NULL, '356', '2017-08-29 00:57:04', '2017-08-29 00:57:04'),
(120, 'MH', 'جزر مارشال', 'Marshall Islands', NULL, '692', '2017-08-29 00:57:04', '2017-08-29 00:57:04'),
(121, 'MR', 'موريتانيا', 'Mauritania', NULL, '222', '2017-08-29 00:57:04', '2017-08-29 00:57:04'),
(122, 'MU', 'موريشيوس', 'Mauritius', NULL, '230', '2017-08-29 00:57:04', '2017-08-29 00:57:04'),
(123, 'YT', 'مايوت', 'Mayotte', NULL, '262', '2017-08-29 00:57:04', '2017-08-29 00:57:04'),
(124, 'MX', 'المكسيك', 'Mexico', NULL, '52', '2017-08-29 00:57:04', '2017-08-29 00:57:04'),
(125, 'FM', 'ولايات ميكرونيزيا الموحدة', 'Micronesia', NULL, '691', '2017-08-29 00:57:04', '2017-08-29 00:57:04'),
(126, 'MD', 'مولدوفا', 'Moldova', NULL, '373', '2017-08-29 00:57:04', '2017-08-29 00:57:04'),
(127, 'MC', 'موناكو', 'Monaco', NULL, '377', '2017-08-29 00:57:04', '2017-08-29 00:57:04'),
(128, 'MN', 'منغوليا', 'Mongolia', NULL, '976', '2017-08-29 00:57:04', '2017-08-29 00:57:04'),
(129, 'ME', 'الجبل الأسود', 'Montenegro', NULL, '382', '2017-08-29 00:57:04', '2017-08-29 00:57:04'),
(130, 'MS', 'مونتسيرات', 'Montserrat', NULL, '1-664', '2017-08-29 00:57:04', '2017-08-29 00:57:04'),
(131, 'MA', 'المغرب', 'Morocco', NULL, '212', '2017-08-29 00:57:04', '2017-08-29 00:57:04'),
(132, 'MZ', 'موزمبيق', 'Mozambique', NULL, '258', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(133, 'MM', 'ميانمار', 'Myanmar', NULL, '95', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(134, 'NA', 'ناميبيا', 'Namibia', NULL, '264', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(135, 'NR', 'ناورو', 'Nauru', NULL, '674', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(136, 'NP', 'نيبال', 'Nepal', NULL, '977', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(137, 'NL', 'هولندا', 'Netherlands', NULL, '31', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(138, 'AN', 'جزر الأنتيل الهولندية', 'Netherlands Antilles', NULL, '599', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(139, 'NC', 'كاليدونيا الجديدة', 'New Caledonia', NULL, '687', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(140, 'NZ', 'نيوزيلندا', 'New Zealand', NULL, '64', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(141, 'NI', 'نيكاراغوا', 'Nicaragua', NULL, '505', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(142, 'NE', 'النيجر', 'Niger', NULL, '227', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(143, 'NG', 'نيجيريا', 'Nigeria', NULL, '234', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(144, 'NU', 'نيوي', 'Niue', NULL, '683', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(145, 'NO', 'النرويج', 'Norway', NULL, '47', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(146, 'OM', 'عمان', 'Oman', NULL, '968', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(147, 'PK', 'باكستان', 'Pakistan', NULL, '92', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(148, 'PW', 'بالاو', 'Palau', NULL, '680', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(149, 'PS', 'فلسطين', 'Palestinian', NULL, '972', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(150, 'PA', 'بناما', 'Panama', NULL, '507', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(151, 'PY', 'باراغواي', 'Paraguay', 'Paraguay', '595', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(152, 'PE', 'بيرو', 'Peru', NULL, '51', '2017-08-29 00:57:05', '2017-08-29 00:57:05'),
(153, 'PH', 'الفلبين', 'Philippines', NULL, '63', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(154, 'PN', 'بيتكيرن', 'Pitcairn', NULL, '870', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(155, 'PL', 'بولندا', 'Poland', NULL, '48', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(156, 'PT', 'البرتغال', 'Portugal', NULL, '351', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(157, 'PR', 'بويرتو ريكو', 'Puerto Rico', NULL, '1-787', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(158, 'QA', 'قطر', 'Qatar', NULL, '974', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(159, 'RO', 'رومانيا', 'Romania', NULL, '40', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(160, 'RU', 'الفيدرالية الروسية', 'Russian Federation', NULL, '7', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(161, 'RW', 'رواندا', 'Rwanda', NULL, '250', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(162, 'SH', 'سانت هيلينا', 'Saint Helena', NULL, '290', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(163, 'KN', 'سانت كيتس ونيفيس', 'Saint Kitts and Nevis', NULL, '1-869', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(164, 'LC', 'سانت لوسيا', 'Saint Lucia', NULL, '1-758', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(165, 'PM', 'سان بيار وميكلون', 'Saint Pierre and Miquelon', NULL, '508', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(166, 'VC', 'سانت فنسنت وجزر غرينادين', 'Saint Vincent and Grenadines', NULL, '1-784', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(167, 'WS', 'ساموا', 'Samoa', NULL, '685', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(168, 'SM', 'سان مارينو', 'San Marino', NULL, '378', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(169, 'ST', 'ساو تومي وبرينسيبي', 'Sao Tome and Principe', NULL, '239', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(170, 'SA', 'المملكة العربية السعودية', 'Saudi Arabia', NULL, '966', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(171, 'SN', 'السنغال', 'Senegal', NULL, '221', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(172, 'RS', 'صربيا', 'Serbia', NULL, '381', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(173, 'SC', 'سيشيل', 'Seychelles', NULL, '248', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(174, 'SL', 'سيرا ليون', 'Sierra Leone', NULL, '232', '2017-08-29 00:57:06', '2017-08-29 00:57:06'),
(175, 'SG', 'سنغافورة', 'Singapore', NULL, '65', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(176, 'SK', 'سلوفاكيا', 'Slovakia', NULL, '421', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(177, 'SI', 'سلوفينيا', 'Slovenia', NULL, '386', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(178, 'SB', 'جزر سليمان', 'Solomon Islands', NULL, '677', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(179, 'SO', 'الصومال', 'Somalia', NULL, '252', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(180, 'ZA', 'جنوب أفريقيا', 'South Africa', NULL, '27', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(181, 'ES', 'إسبانيا', 'Spain', NULL, '34', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(182, 'LK', 'سيريلانكا', 'Sri Lanka', NULL, '94', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(183, 'SD', 'السودان', 'Sudan', NULL, '249', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(184, 'SR', 'سورينام', 'Suriname', NULL, '597', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(185, 'SJ', 'جزر سفالبارد وجان ماين', 'Svalbard and Jan Mayen Islands', NULL, '47', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(186, 'SZ', 'سوازيلاند', 'Swaziland', NULL, '268', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(187, 'SE', 'السويد', 'Sweden', NULL, '46', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(188, 'CH', 'سويسرا', 'Switzerland', NULL, '41', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(189, 'SY', 'سوريا', 'Syrian Arab Republic', NULL, '963', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(190, 'TW', 'تايوان، جمهورية الصين', 'Taiwan, Republic of China', NULL, '886', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(191, 'TJ', 'طاجيكستان', 'Tajikistan', NULL, '992', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(192, 'TZ', 'تنزانيا', 'Tanzania', NULL, '255', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(193, 'TH', 'تايلاند', 'Thailand', NULL, '66', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(194, 'TG', 'توغو', 'Togo', NULL, '228', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(195, 'TK', 'توكيلاو', 'Tokelau', NULL, '690', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(196, 'TO', 'تونغا', 'Tonga', NULL, '676', '2017-08-29 00:57:07', '2017-08-29 00:57:07'),
(197, 'TT', 'ترينداد وتوباغو', 'Trinidad and Tobago', NULL, '1-868', '2017-08-29 00:57:08', '2017-08-29 00:57:08'),
(198, 'TN', 'تونس', 'Tunisia', NULL, '216', '2017-08-29 00:57:08', '2017-08-29 00:57:08'),
(199, 'TR', 'تركيا', 'Turkey', NULL, '90', '2017-08-29 00:57:08', '2017-08-29 00:57:08'),
(200, 'TM', 'تركمانستان', 'Turkmenistan', NULL, '993', '2017-08-29 00:57:08', '2017-08-29 00:57:08'),
(201, 'TC', 'جزر تركس وكايكوس', 'Turks and Caicos Islands', NULL, '1-649', '2017-08-29 00:57:08', '2017-08-29 00:57:08'),
(202, 'TV', 'توفالو', 'Tuvalu', NULL, '688', '2017-08-29 00:57:08', '2017-08-29 00:57:08'),
(203, 'UG', 'أوغندا', 'Uganda', NULL, '256', '2017-08-29 00:57:08', '2017-08-29 00:57:08'),
(204, 'UA', 'أوكرانيا', 'Ukraine', NULL, '380', '2017-08-29 00:57:08', '2017-08-29 00:57:08'),
(205, 'AE', 'الإمارات العربية المتحدة', 'United Arab Emirates', NULL, '971', '2017-08-29 00:57:08', '2017-08-29 00:57:08'),
(206, 'GB', 'المملكة المتحدة', 'United Kingdom', NULL, '44', '2017-08-29 00:57:08', '2017-08-29 00:57:08'),
(207, 'US', 'الولايات المتحدة الأمريكية', 'United States of America', NULL, '1', '2017-08-29 00:57:08', '2017-08-29 00:57:08'),
(208, 'UY', 'أوروغواي', 'Uruguay', 'Uruguay', '598', '2017-08-29 00:57:08', '2017-08-29 00:57:08'),
(209, 'UZ', 'أوزبكستان', 'Uzbekistan', NULL, '998', '2017-08-29 00:57:08', '2017-08-29 00:57:08'),
(210, 'VU', 'فانواتو', 'Vanuatu', NULL, '678', '2017-08-29 00:57:08', '2017-08-29 00:57:08'),
(211, 'VE', 'فنزويلا', 'Venezuela', NULL, '58', '2017-08-29 00:57:08', '2017-08-29 00:57:08'),
(212, 'VN', 'فيتنام', 'Viet Nam', NULL, '84', '2017-08-29 00:57:08', '2017-08-29 00:57:08'),
(213, 'WF', 'واليس وفوتونا', 'Wallis and Futuna Islands', NULL, '681', '2017-08-29 00:57:08', '2017-08-29 00:57:08'),
(214, 'YE', 'اليمن', 'Yemen', NULL, '967', '2017-08-29 00:57:08', '2017-08-29 00:57:08'),
(215, 'ZM', 'زامبيا', 'Zambia', NULL, '260', '2017-08-29 00:57:08', '2017-08-29 00:57:08'),
(216, 'ZW', 'زيمبابوي', 'Zimbabwe', NULL, '263', '2017-08-29 00:57:09', '2017-08-29 00:57:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_events`
--

CREATE TABLE `smartend_events` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_ltm_translations`
--

CREATE TABLE `smartend_ltm_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_maps`
--

CREATE TABLE `smartend_maps` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(11) NOT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_ar` text COLLATE utf8mb4_unicode_ci,
  `details_en` text COLLATE utf8mb4_unicode_ci,
  `icon` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_menus`
--

CREATE TABLE `smartend_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `row_no` int(11) NOT NULL,
  `father_id` int(11) NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_es` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `smartend_menus`
--

INSERT INTO `smartend_menus` (`id`, `row_no`, `father_id`, `title_ar`, `title_en`, `title_es`, `status`, `type`, `cat_id`, `link`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'القائمة الرئيسية', 'Main Menu', 'Menu Principal', 1, 0, 0, '', 1, NULL, '2017-08-29 00:57:09', '2017-08-29 00:57:09'),
(2, 2, 0, 'روابط سريعة', 'Quick Links', 'Accesos Rapidos', 1, 0, 0, '', 1, NULL, '2017-08-29 00:57:09', '2017-08-29 00:57:09'),
(3, 1, 1, 'الرئيسية', 'Home', 'Inicio', 1, 1, 0, 'home', 1, NULL, '2017-08-29 00:57:09', '2017-08-29 00:57:09'),
(4, 2, 1, 'من نحن', 'About', 'Nosotros', 1, 1, 0, 'topic/about', 1, NULL, '2017-08-29 00:57:09', '2017-08-29 00:57:09'),
(5, 3, 1, 'خدماتنا', 'Services', 'Servicios', 1, 3, 2, '', 1, NULL, '2017-08-29 00:57:09', '2017-08-29 00:57:09'),
(6, 4, 1, 'أخبارنا', 'News', 'Noticias', 1, 2, 3, '', 1, NULL, '2017-08-29 00:57:09', '2017-08-29 00:57:09'),
(7, 5, 1, 'الصور', 'Photos', 'Imagenes', 1, 2, 4, '', 1, NULL, '2017-08-29 00:57:09', '2017-08-29 00:57:09'),
(8, 6, 1, 'الفيديو', 'Videos', 'Videos', 1, 3, 5, '', 1, NULL, '2017-08-29 00:57:09', '2017-08-29 00:57:09'),
(9, 7, 1, NULL, 'Audio', 'Audio', 0, 3, 6, NULL, 1, 1, '2017-08-29 00:57:09', '2017-08-29 01:00:37'),
(10, 8, 1, 'المنتجات', 'Products', 'Productos', 1, 3, 8, '', 1, NULL, '2017-08-29 00:57:09', '2017-08-29 00:57:09'),
(11, 9, 1, 'المدونة', 'Blog', 'Blog', 1, 2, 7, '', 1, NULL, '2017-08-29 00:57:09', '2017-08-29 00:57:09'),
(12, 10, 1, 'اتصل بنا', 'Contact', 'Contacto', 1, 1, 0, 'contact', 1, NULL, '2017-08-29 00:57:09', '2017-08-29 00:57:09'),
(13, 1, 2, 'الرئيسية', 'Home', 'Inicio', 1, 1, 0, 'home', 1, NULL, '2017-08-29 00:57:09', '2017-08-29 00:57:09'),
(14, 2, 2, 'من نحن', 'About Us', 'Sobre Nosotros', 1, 1, 0, 'topic/about', 1, NULL, '2017-08-29 00:57:09', '2017-08-29 00:57:09'),
(15, 3, 2, 'المدونة', 'Blog', 'Blog', 1, 2, 7, '', 1, NULL, '2017-08-29 00:57:09', '2017-08-29 00:57:09'),
(16, 4, 2, 'الخصوصية', 'Privacy', 'Privacidad', 1, 1, 0, 'topic/privacy', 1, NULL, '2017-08-29 00:57:10', '2017-08-29 00:57:10'),
(17, 5, 2, 'الشروط والأحكام', 'Terms & Conditions', 'Terminos y Condiciones', 1, 1, 0, 'topic/terms', 1, NULL, '2017-08-29 00:57:10', '2017-08-29 00:57:10'),
(18, 6, 2, 'اتصل بنا', 'Contact Us', 'Contáctenos', 1, 1, 0, 'contact', 1, NULL, '2017-08-29 00:57:10', '2017-08-29 00:57:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_migrations`
--

CREATE TABLE `smartend_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `smartend_migrations`
--

INSERT INTO `smartend_migrations` (`id`, `migration`, `batch`) VALUES
(986, '2014_04_02_193005_create_translations_table', 1),
(987, '2014_10_12_000000_create_users_table', 1),
(988, '2014_10_12_100000_create_password_resets_table', 1),
(989, '2017_02_16_230800_create_webmaster_settings_table', 1),
(990, '2017_02_16_230846_create_webmaster_sections_table', 1),
(991, '2017_02_16_230900_create_webmaster_banners_table', 1),
(992, '2017_02_16_231036_create_webmails_groups_table', 1),
(993, '2017_02_16_231044_create_webmails_files_table', 1),
(994, '2017_02_16_231053_create_webmails_table', 1),
(995, '2017_02_16_231114_create_topics_table', 1),
(996, '2017_02_16_231142_create_settings_table', 1),
(997, '2017_02_16_231230_create_sections_table', 1),
(998, '2017_02_16_231240_create_photos_table', 1),
(999, '2017_02_16_231248_create_menus_table', 1),
(1000, '2017_02_16_231259_create_maps_table', 1),
(1001, '2017_02_16_231306_create_events_table', 1),
(1002, '2017_02_16_231317_create_countries_table', 1),
(1003, '2017_02_16_231327_create_contacts_groups_table', 1),
(1004, '2017_02_16_231339_create_contacts_table', 1),
(1005, '2017_02_16_231346_create_comments_table', 1),
(1006, '2017_02_16_231352_create_banners_table', 1),
(1007, '2017_02_16_231359_create_analytics_visitors_table', 1),
(1008, '2017_02_16_231409_create_analytics_pages_table', 1),
(1009, '2017_02_28_095712_create_permissions_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_password_resets`
--

CREATE TABLE `smartend_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_permissions`
--

CREATE TABLE `smartend_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view_status` tinyint(4) NOT NULL DEFAULT '0',
  `add_status` tinyint(4) NOT NULL DEFAULT '0',
  `edit_status` tinyint(4) NOT NULL DEFAULT '0',
  `delete_status` tinyint(4) NOT NULL DEFAULT '0',
  `analytics_status` tinyint(4) NOT NULL DEFAULT '0',
  `inbox_status` tinyint(4) NOT NULL DEFAULT '0',
  `newsletter_status` tinyint(4) NOT NULL DEFAULT '0',
  `calendar_status` tinyint(4) NOT NULL DEFAULT '0',
  `banners_status` tinyint(4) NOT NULL DEFAULT '0',
  `settings_status` tinyint(4) NOT NULL DEFAULT '0',
  `webmaster_status` tinyint(4) NOT NULL DEFAULT '0',
  `data_sections` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `smartend_permissions`
--

INSERT INTO `smartend_permissions` (`id`, `name`, `view_status`, `add_status`, `edit_status`, `delete_status`, `analytics_status`, `inbox_status`, `newsletter_status`, `calendar_status`, `banners_status`, `settings_status`, `webmaster_status`, `data_sections`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Webmaster', 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '1,2,3,4,5,6,7,8', 1, 1, NULL, '2017-08-29 00:56:56', '2017-08-29 00:56:56'),
(2, 'Gerente Web', 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, '1,2,3,4,5,6,7,8', 1, 1, NULL, '2017-08-29 00:56:56', '2017-08-29 00:56:56'),
(3, 'Usuario Limitado', 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, '1,2,3,4,5,6,7,8', 1, 1, NULL, '2017-08-29 00:56:56', '2017-08-29 00:56:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_photos`
--

CREATE TABLE `smartend_photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(11) NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_sections`
--

CREATE TABLE `smartend_sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_es` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `visits` int(11) NOT NULL,
  `webmaster_id` int(11) NOT NULL,
  `father_id` int(11) NOT NULL,
  `row_no` int(11) NOT NULL,
  `seo_title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title_es` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_es` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_es` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `smartend_sections`
--

INSERT INTO `smartend_sections` (`id`, `title_ar`, `title_en`, `title_es`, `photo`, `icon`, `status`, `visits`, `webmaster_id`, `father_id`, `row_no`, `seo_title_ar`, `seo_title_en`, `seo_title_es`, `seo_description_ar`, `seo_description_en`, `seo_description_es`, `seo_keywords_ar`, `seo_keywords_en`, `seo_keywords_es`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'Categoría 1', NULL, 'fa-american-sign-language-interpreting', 1, 1, 8, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-08-29 01:17:21', '2017-08-29 01:18:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_settings`
--

CREATE TABLE `smartend_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_title_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_title_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_title_es` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_desc_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_desc_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_desc_es` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_keywords_ar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_keywords_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_keywords_es` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_webmails` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notify_messages_status` tinyint(4) DEFAULT NULL,
  `notify_comments_status` tinyint(4) DEFAULT NULL,
  `notify_orders_status` tinyint(4) DEFAULT NULL,
  `site_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_status` tinyint(4) NOT NULL,
  `close_msg` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link4` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link5` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link6` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link7` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link8` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link9` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link10` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t1_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t1_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t1_es` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t4` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t5` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t6` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t7_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t7_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t7_es` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `style_logo_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_logo_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_logo_es` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_fav` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_apple` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_color1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_color2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_type` tinyint(4) DEFAULT NULL,
  `style_bg_type` tinyint(4) DEFAULT NULL,
  `style_bg_pattern` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_bg_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_bg_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_subscribe` tinyint(4) DEFAULT NULL,
  `style_footer` tinyint(4) DEFAULT NULL,
  `style_footer_bg` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_preload` tinyint(4) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `smartend_settings`
--

INSERT INTO `smartend_settings` (`id`, `site_title_ar`, `site_title_en`, `site_title_es`, `site_desc_ar`, `site_desc_en`, `site_desc_es`, `site_keywords_ar`, `site_keywords_en`, `site_keywords_es`, `site_webmails`, `notify_messages_status`, `notify_comments_status`, `notify_orders_status`, `site_url`, `site_status`, `close_msg`, `social_link1`, `social_link2`, `social_link3`, `social_link4`, `social_link5`, `social_link6`, `social_link7`, `social_link8`, `social_link9`, `social_link10`, `contact_t1_ar`, `contact_t1_en`, `contact_t1_es`, `contact_t3`, `contact_t4`, `contact_t5`, `contact_t6`, `contact_t7_ar`, `contact_t7_en`, `contact_t7_es`, `style_logo_ar`, `style_logo_en`, `style_logo_es`, `style_fav`, `style_apple`, `style_color1`, `style_color2`, `style_type`, `style_bg_type`, `style_bg_pattern`, `style_bg_color`, `style_bg_image`, `style_subscribe`, `style_footer`, `style_footer_bg`, `style_preload`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'اسم الموقع', 'Site Title', 'Titulo del sitio', 'وصف الموقع الإلكتروني ونبذة قصيره عنه', 'Website description and some little information about it', 'Descripción del sitio web y un poco de información sobre el mismo', 'كلمات، دلالية، موقع، موقع إلكتروني', 'key, words, website, web', 'Clave, palabra, website, web', 'info@dominio.com', 1, 1, 1, 'http://www.paginaweb.com.ar/', 1, 'El sitio esta actualmente en mantenimiento \n<h1>Volveremos Pronto</h1>', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', 'المبني - اسم الشارع - المدينة - الدولة', 'Building, Street name, City, Country', 'Calle Altura, Ciudad, Provincia', '+(xxx) 0xxxxxxx', '+(xxx) 0xxxxxxx', '+(xxx) 0xxxxxxx', 'info@sitename.com', 'من الأحد إلى الخميس 08:00 ص - 05:00 م', 'Sunday to Thursday 08:00 AM to 05:00 PM', 'Luneas a Viernes de 08:00 AM a 05:00 PM', NULL, '15039576102214.png', '15039576105189.png', '15039576107598.png', '15039576102276.png', '#3494c8', '#2e3e4e', 0, 0, NULL, '#2e3e4e', NULL, 1, 1, NULL, 1, 1, 1, '2017-08-29 00:56:57', '2017-08-29 01:00:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_topics`
--

CREATE TABLE `smartend_topics` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_es` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_ar` longtext COLLATE utf8mb4_unicode_ci,
  `details_en` longtext COLLATE utf8mb4_unicode_ci,
  `details_es` longtext COLLATE utf8mb4_unicode_ci,
  `date` date DEFAULT NULL,
  `video_type` tinyint(4) DEFAULT NULL,
  `photo_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attach_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_file` text COLLATE utf8mb4_unicode_ci,
  `audio_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `visits` int(11) NOT NULL,
  `webmaster_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `row_no` int(11) NOT NULL,
  `seo_title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title_es` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_es` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_es` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `smartend_topics`
--

INSERT INTO `smartend_topics` (`id`, `title_ar`, `title_en`, `title_es`, `details_ar`, `details_en`, `details_es`, `date`, `video_type`, `photo_file`, `attach_file`, `video_file`, `audio_file`, `icon`, `status`, `visits`, `webmaster_id`, `section_id`, `row_no`, `seo_title_ar`, `seo_title_en`, `seo_title_es`, `seo_description_ar`, `seo_description_en`, `seo_description_es`, `seo_keywords_ar`, `seo_keywords_en`, `seo_keywords_es`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, NULL, 'About Us', 'Sobre Nosotros', NULL, 'It is a long established fact that a reader will be distracted by the readable content of a page.', 'Es un hecho establecido desde hace mucho tiempo que un lector se distraiga por el contenido legible de una página.', '2017-08-28', NULL, NULL, NULL, NULL, NULL, NULL, 1, 4, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-08-29 00:57:10', '2017-08-29 01:18:53'),
(2, 'اتصل بنا', 'Contact Us', 'Contáctenos', '', '', NULL, '2017-08-28', NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 1, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-08-29 00:57:10', '2017-08-29 01:18:13'),
(3, 'الخصوصية', 'Privacy', NULL, 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', 'Es un hecho establecido desde hace mucho tiempo que un lector se distraiga por el contenido legible de una página.', '2017-08-28', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-08-29 00:57:10', '2017-08-29 00:57:10'),
(4, 'الشروط والأحكام', 'Terms & Conditions', 'Terminos y Condiciones', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', 'Es un hecho establecido desde hace mucho tiempo que un lector se distraiga por el contenido legible de una página.', '2017-08-28', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-08-29 00:57:11', '2017-08-29 00:57:11'),
(5, NULL, NULL, 'Servicio 1', NULL, NULL, 'Texto de la pagina del servicio 1', '2017-08-28', NULL, '15039591584505.jpg', NULL, NULL, NULL, NULL, 1, 3, 2, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-08-29 01:15:16', '2017-08-29 01:26:01'),
(6, NULL, NULL, 'Servicio 2', NULL, NULL, '<div><span style="font-size: 13.92px;">Texto de la pagina del servicio 2</span><br></div>', '2017-08-28', NULL, '15039591855527.jpg', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-08-29 01:15:34', '2017-08-29 01:26:31'),
(7, NULL, NULL, 'Producto 1', NULL, NULL, '<div>Detalles del producto 1</div>', '2017-08-28', NULL, '15039586885929.png', NULL, NULL, NULL, NULL, 1, 1, 8, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-08-29 01:18:08', '2017-08-29 01:18:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_users`
--

CREATE TABLE `smartend_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `connect_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `connect_password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `smartend_users`
--

INSERT INTO `smartend_users` (`id`, `name`, `email`, `password`, `photo`, `permissions_id`, `status`, `connect_email`, `connect_password`, `remember_token`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', 'admin@admin.com', '$2y$10$u81yinkHsM3jhzIChaiRiuLiaPQtnDAXY/aDYVg/DTCypDQ6QHjI6', NULL, 1, 1, NULL, NULL, NULL, 1, NULL, '2017-08-29 00:56:56', '2017-08-29 00:56:56'),
(2, 'Gerente', 'gerente@gerente.com', '$2y$10$OgW8dHelL8gVM.qrOqq1u.TFbrvkpMAwu5oDmSvHWrf6Pudg/Ki3i', NULL, 2, 1, NULL, NULL, NULL, 1, NULL, '2017-08-29 00:56:56', '2017-08-29 00:56:56'),
(3, 'Usuario', 'usuario@usuario.com', '$2y$10$xE2KSffcV9RZpFx1Kluwnu0J229Z26qtRr2z/XwP15JcqHyDr3Zh2', NULL, 3, 1, NULL, NULL, NULL, 1, NULL, '2017-08-29 00:56:56', '2017-08-29 00:56:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_webmails`
--

CREATE TABLE `smartend_webmails` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `father_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci,
  `date` datetime NOT NULL,
  `from_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_cc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_bcc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `flag` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_webmails_files`
--

CREATE TABLE `smartend_webmails_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `webmail_id` int(11) NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_webmails_groups`
--

CREATE TABLE `smartend_webmails_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_webmaster_banners`
--

CREATE TABLE `smartend_webmaster_banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `row_no` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `desc_status` tinyint(4) NOT NULL,
  `link_status` tinyint(4) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `icon_status` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `smartend_webmaster_banners`
--

INSERT INTO `smartend_webmaster_banners` (`id`, `row_no`, `name`, `width`, `height`, `desc_status`, `link_status`, `type`, `icon_status`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'homeBanners', 1600, 500, 1, 1, 1, 0, 1, 1, NULL, '2017-08-29 00:56:56', '2017-08-29 00:56:56'),
(2, 2, 'textBanners', 330, 330, 1, 1, 0, 1, 1, 1, NULL, '2017-08-29 00:56:56', '2017-08-29 00:56:56'),
(3, 3, 'sideBanners', 330, 330, 0, 1, 1, 0, 1, 1, NULL, '2017-08-29 00:56:56', '2017-08-29 00:56:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_webmaster_sections`
--

CREATE TABLE `smartend_webmaster_sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `row_no` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL,
  `sections_status` tinyint(4) NOT NULL,
  `comments_status` tinyint(4) NOT NULL,
  `date_status` tinyint(4) NOT NULL,
  `longtext_status` tinyint(4) NOT NULL,
  `editor_status` tinyint(4) NOT NULL,
  `attach_file_status` tinyint(4) NOT NULL,
  `multi_images_status` tinyint(4) NOT NULL,
  `section_icon_status` tinyint(4) NOT NULL,
  `icon_status` tinyint(4) NOT NULL,
  `maps_status` tinyint(4) NOT NULL,
  `order_status` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `smartend_webmaster_sections`
--

INSERT INTO `smartend_webmaster_sections` (`id`, `row_no`, `name`, `type`, `sections_status`, `comments_status`, `date_status`, `longtext_status`, `editor_status`, `attach_file_status`, `multi_images_status`, `section_icon_status`, `icon_status`, `maps_status`, `order_status`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'sitePages', 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, NULL, '2017-08-29 00:56:57', '2017-08-29 00:56:57'),
(2, 2, 'services', 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, NULL, '2017-08-29 00:56:57', '2017-08-29 00:56:57'),
(3, 3, 'news', 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, NULL, '2017-08-29 00:56:57', '2017-08-29 00:56:57'),
(4, 4, 'photos', 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, NULL, '2017-08-29 00:56:57', '2017-08-29 00:56:57'),
(5, 5, 'videos', 2, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, NULL, '2017-08-29 00:56:57', '2017-08-29 00:56:57'),
(6, 6, 'sounds', 3, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, NULL, '2017-08-29 00:56:57', '2017-08-29 00:56:57'),
(7, 7, 'blog', 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, NULL, '2017-08-29 00:56:57', '2017-08-29 00:56:57'),
(8, 8, 'products', 0, 2, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, NULL, '2017-08-29 00:56:57', '2017-08-29 00:56:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `smartend_webmaster_settings`
--

CREATE TABLE `smartend_webmaster_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `ar_box_status` tinyint(4) NOT NULL,
  `en_box_status` tinyint(4) NOT NULL,
  `es_box_status` tinyint(4) NOT NULL,
  `seo_status` tinyint(4) NOT NULL,
  `analytics_status` tinyint(4) NOT NULL,
  `banners_status` tinyint(4) NOT NULL,
  `inbox_status` tinyint(4) NOT NULL,
  `calendar_status` tinyint(4) NOT NULL,
  `settings_status` tinyint(4) NOT NULL,
  `newsletter_status` tinyint(4) NOT NULL,
  `members_status` tinyint(4) NOT NULL,
  `orders_status` tinyint(4) NOT NULL,
  `shop_status` tinyint(4) NOT NULL,
  `shop_settings_status` tinyint(4) NOT NULL,
  `default_currency_id` int(11) NOT NULL,
  `languages_count` int(11) NOT NULL,
  `latest_news_section_id` int(11) NOT NULL,
  `header_menu_id` int(11) NOT NULL,
  `footer_menu_id` int(11) NOT NULL,
  `home_banners_section_id` int(11) NOT NULL,
  `home_text_banners_section_id` int(11) NOT NULL,
  `side_banners_section_id` int(11) NOT NULL,
  `contact_page_id` int(11) NOT NULL,
  `newsletter_contacts_group` int(11) NOT NULL,
  `new_comments_status` tinyint(4) NOT NULL,
  `home_content1_section_id` int(11) NOT NULL,
  `home_content2_section_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `smartend_webmaster_settings`
--

INSERT INTO `smartend_webmaster_settings` (`id`, `ar_box_status`, `en_box_status`, `es_box_status`, `seo_status`, `analytics_status`, `banners_status`, `inbox_status`, `calendar_status`, `settings_status`, `newsletter_status`, `members_status`, `orders_status`, `shop_status`, `shop_settings_status`, `default_currency_id`, `languages_count`, `latest_news_section_id`, `header_menu_id`, `footer_menu_id`, `home_banners_section_id`, `home_text_banners_section_id`, `side_banners_section_id`, `contact_page_id`, `newsletter_contacts_group`, `new_comments_status`, `home_content1_section_id`, `home_content2_section_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 2, 3, 1, 2, 1, 2, 3, 2, 1, 1, 7, 4, 1, 1, '2017-08-29 00:56:56', '2017-08-29 01:01:41');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `smartend_analytics_pages`
--
ALTER TABLE `smartend_analytics_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_analytics_visitors`
--
ALTER TABLE `smartend_analytics_visitors`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_banners`
--
ALTER TABLE `smartend_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_comments`
--
ALTER TABLE `smartend_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_contacts`
--
ALTER TABLE `smartend_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_contacts_groups`
--
ALTER TABLE `smartend_contacts_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_countries`
--
ALTER TABLE `smartend_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_events`
--
ALTER TABLE `smartend_events`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_ltm_translations`
--
ALTER TABLE `smartend_ltm_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_maps`
--
ALTER TABLE `smartend_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_menus`
--
ALTER TABLE `smartend_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_migrations`
--
ALTER TABLE `smartend_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_password_resets`
--
ALTER TABLE `smartend_password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `smartend_permissions`
--
ALTER TABLE `smartend_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_photos`
--
ALTER TABLE `smartend_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_sections`
--
ALTER TABLE `smartend_sections`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_settings`
--
ALTER TABLE `smartend_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_topics`
--
ALTER TABLE `smartend_topics`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_users`
--
ALTER TABLE `smartend_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `smartend_webmails`
--
ALTER TABLE `smartend_webmails`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_webmails_files`
--
ALTER TABLE `smartend_webmails_files`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_webmails_groups`
--
ALTER TABLE `smartend_webmails_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_webmaster_banners`
--
ALTER TABLE `smartend_webmaster_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_webmaster_sections`
--
ALTER TABLE `smartend_webmaster_sections`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `smartend_webmaster_settings`
--
ALTER TABLE `smartend_webmaster_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `smartend_analytics_pages`
--
ALTER TABLE `smartend_analytics_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT de la tabla `smartend_analytics_visitors`
--
ALTER TABLE `smartend_analytics_visitors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `smartend_banners`
--
ALTER TABLE `smartend_banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `smartend_comments`
--
ALTER TABLE `smartend_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `smartend_contacts`
--
ALTER TABLE `smartend_contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `smartend_contacts_groups`
--
ALTER TABLE `smartend_contacts_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `smartend_countries`
--
ALTER TABLE `smartend_countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=217;
--
-- AUTO_INCREMENT de la tabla `smartend_events`
--
ALTER TABLE `smartend_events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `smartend_ltm_translations`
--
ALTER TABLE `smartend_ltm_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `smartend_maps`
--
ALTER TABLE `smartend_maps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `smartend_menus`
--
ALTER TABLE `smartend_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `smartend_migrations`
--
ALTER TABLE `smartend_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1010;
--
-- AUTO_INCREMENT de la tabla `smartend_permissions`
--
ALTER TABLE `smartend_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `smartend_photos`
--
ALTER TABLE `smartend_photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `smartend_sections`
--
ALTER TABLE `smartend_sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `smartend_settings`
--
ALTER TABLE `smartend_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `smartend_topics`
--
ALTER TABLE `smartend_topics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `smartend_users`
--
ALTER TABLE `smartend_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `smartend_webmails`
--
ALTER TABLE `smartend_webmails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `smartend_webmails_files`
--
ALTER TABLE `smartend_webmails_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `smartend_webmails_groups`
--
ALTER TABLE `smartend_webmails_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `smartend_webmaster_banners`
--
ALTER TABLE `smartend_webmaster_banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `smartend_webmaster_sections`
--
ALTER TABLE `smartend_webmaster_sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `smartend_webmaster_settings`
--
ALTER TABLE `smartend_webmaster_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
