<?php

return [

    // Language Settings
    'direction' => 'dirección',
    'code' => 'es',
    'lang' => 'ES',
    'left' => 'izquierda',
    'right' => 'derecha',
    'arabicBox' => '<small>[ العربية ]</small>',
    'englishBox' => '<small>[ English ]</small>',
    'spanishBox' => '<small>[ Español ]</small>',
    'rtl' => 'rtl',
    'ltr' => 'ltr',
    'boxCode' => 'es',
    'boxCodeOther' => 'en',
    'translations' => 'Traducciones',
    'calendarLanguage' => 'es',


    // Main Menu & General Titles
    'home' => 'Inicio',
    'main' => 'Principal',
    'siteData' => 'Secciones del Sitio Web',
    'settings' => 'Ajustes',
    'dashboard' => 'Panel Principal',
    'visitorsAnalytics' => 'Reportes',
    'visitorsAnalyticsBydate' => 'Por fecha',
    'visitorsAnalyticsByCity' => 'Ciudad',
    'visitorsAnalyticsByRegion' => 'Region',
    'visitorsAnalyticsByAddress' => 'Dirección',
    'visitorsAnalyticsByCountry' => 'Pais',
    'visitorsAnalyticsByOperatingSystem' => 'Sistema Operativo',
    'visitorsAnalyticsByBrowser' => 'Navegador',
    'visitorsAnalyticsByScreenResolution' => 'Resolución de pantalla',
    'visitorsAnalyticsByReachWay' => 'Sitio web de ingreso',
    'visitorsAnalyticsByHostName' => 'Nombre de host',
    'visitorsAnalyticsByOrganization' => 'Organización',
    'visitorsAnalyticsVisitorsHistory' => 'Historial de visitantes',
    'visitorsAnalyticsIPInquiry' => 'Investigación de IP',
    'visitorsAnalyticsLastVisit' => 'Ultima Visita',
    'members' => 'Miembros',
    'adsBanners' => 'Banners',
    'siteInbox' => 'Bandeja de entrada',
    'newsletter' => 'Contactos',
    'calendar' => 'Calendario',
    'generalSiteSettings' => 'Ajustes',
    'webmasterTools' => 'Webmaster',
    'generalSettings' => 'Configuración General',
    'siteSectionsSettings' => 'Secciones del sitio',
    'adsBannersSettings' => 'Ajustes de Banners',
    'languageSettings' => 'Configuraciones de languaje',
    'siteMenus' => 'Menus del sitio',
    'usersPermissions' => 'Usuarios y Permisos',
    'shoppingCart' => 'Carrito de compras',
    'orders' => 'Pedidos',

    // General
    'logout' => 'Cerrar Sesion',
    'checkAll' => 'Compruebe todo',
    'profile' => 'Perfil',
    'search' => 'Buscar',
    'new' => 'Nuevo',
    'add' => '&nbsp;&nbsp;Agregar&nbsp;&nbsp;',
    'update' => 'Actualizar',
    'save' => 'Guardar',
    'saveOrder' => 'Guardar Pedido',
    'edit' => 'Editar',
    'delete' => 'Eliminar',
    'undoDelete' => 'Deshacer eliminación',
    'cancel' => 'Cancelar',
    'apply' => 'Aplicar',
    'active' => 'Habilitado',
    'notActive' => 'Deshabilitado',
    'status' => 'Estado',
    'visits' => 'Visitas',
    'contents' => 'Contenido',
    'options' => 'Opciones',
    'showing' => 'Mostrar',
    'bulkAction' => 'Acción masiva',
    'activeSelected' => 'Activar seleccionado',
    'blockSelected' => 'Bloquear seleccionado',
    'deleteSelected' => 'Eliminar seleccionado',
    'of' => 'de',
    'records' => 'registros',
    'confirmation' => 'Confirmación',
    'yes' => 'Si',
    'no' => 'No',
    'confirmationDeleteMsg' => '¿Estas seguro que quieres borrarlo?',
    'sectionsOf' => 'Categorias de ',
    'comments' => 'Comentarios',
    'videoTypes' => 'Extensiones: .mp4, .ogv, .webm',
    'imagesTypes' => 'Extensiones: .png, .jpg, .jpeg, .gif',
    'audioTypes' => 'Extensiones: .mp3, .wav',
    'attachTypes' => 'Extensiones: (* Todas)',

    //Error messages
    'addDone' => 'Nuevo registro agregado correctamente',
    'saveDone' => 'Las modificaciones se han guardado correctamente',
    'deleteDone' => 'Los datos se eliminaron correctamente',
    'erorr' => 'Hubo un error, por favor intentelo nuevamente',
    'noData' => 'No hay información disponible',

    // Webmaster Settings
    'appsSettings' => 'Apps Activas',
    'fieldsSettings' => 'Configuración de Campos',
    'frontSettings' => 'Ajustes del sitio',
    'arabicLanguageFields' => 'Campos en Arabe',
    'englishLanguageFields' => 'Campos en Ingles',
    'seoTab' => 'SEO Tab',
    'seoTabTitle' => 'SEO Ajustes',
    'defaultCurrency' => 'Moneda predeterminada',
    'commentsStatus' => 'Comentarios Estado',
    'automaticPublish' => 'Publicar Automaticamente',
    'manualByAdmin' => 'Manual por Administrador',
    'headerMenu' => 'Menú del encabezado',
    'footerMenu' => 'Pie de página',
    'homeSlideBanners' => 'Diapositivas del Banners de Página de Inicio',
    'homeTextBanners' => 'Texto de Banners en Página de Inicio',
    'sideBanners' => 'Banners de Páginas interiores',
    'none' => 'Ninguna',
    'newsletterGroup' => 'Boletín de noticias (Dentro de los contactos)',
    'homeRow1' => 'Contenido de la página de inicio FILA 1',
    'homeRow2' => 'Contenido de la página de inicio FILA 2',
    'homeRow3' => 'Último tema (dentro del pie de página)',
    'contactPageId' => 'Contáctenos Página (Detalles y Mapas)',
    'activeLanguages' => 'Idiomas Activos',
    'activeLanguages1' => 'Un idioma ( Por defecto en .env )',
    'activeLanguages2' => 'Todos los idiomas',

    // Webmaster Banners
    'homeBanners' => 'Banner Página Principal',
    'textBanners' => 'Banner de Texto',
    'sideBanners' => 'Banner Lateral',
    'sectionName' => 'Título de sección',
    'sectionType' => 'Tipo de sección',
    'sectionNew' => 'Nueva Sección',
    'sectionEdit' => 'Editar Sección',
    'sectionIcon' => 'Icono',
    'sectionFather' => 'Sección Padre',
    'sectionNoFather' => 'Sin sección padre',
    'size' => 'Tamaño',
    'width' => 'Ancho',
    'height' => 'Altura',
    'descriptionBox' => 'Box de descripción',
    'linkBox' => 'Box de URL',
    'sectionTypeCode' => 'Codigo / Texto',
    'sectionTypePhoto' => 'Imagen',
    'sectionTypeVideo' => 'Video',
    'langVar' => 'Variable de Idioma',

    // Webmaster Sections
    'sitePages' => 'Página',
    'sitePagess' => 'Páginas',
    'photos' => 'Imagen',
    'photoss' => 'Imagenes',
    'blog' => 'Blog',
    'blogs' => 'Blog',
    'services' => 'Servicio',
    'servicess' => 'Servicios',
    'news' => 'Noticia',
    'newss' => 'Noticias',
    'videos' => 'Video',
    'videoss' => 'Videos',
    'sounds' => 'Audio',
    'soundss' => 'Audios',
    'products' => 'Producto',
    'productss' => 'Productos',
    'typeTextPages' => 'Páginas',
    'typePhotos' => 'Imagenes',
    'typeVideos' => 'Videos',
    'typeSounds' => 'Audio',
    'hasCategories' => 'Categorias',
    'reviewsAvailable' => 'Comentarios Permitidos',
    'dateField' => 'Campo de fecha',
    'longTextField' => 'Campo de texto largo',
    'allowEditor' => 'Permitir Ediciones',
    'attachFileField' => 'Campo de Archivo Adjunto',
    'additionalImages' => 'Imagenes Adicionales',
    'attachGoogleMaps' => 'Adjuntar Google Maps',
    'attachOrderForm' => 'Adjuntar formulario de pedido',
    'withoutCategories' => 'Sin Categorias',
    'mainCategoriesOnly' => 'Categorias principales solamente',
    'mainAndSubCategories' => 'Categorias principales y sub categorias',
    'sectionIconPicker' => 'Icono de Sección (Picker)',
    'topicsIconPicker' => 'Iconos de Temas (Picker)',
    'iconPicker' => 'Icono (Picker)',

    // Settings
    'siteInfoSettings' => 'Informacion del sitio',
    'siteStatusSettings' => 'Estado del sitio',
    'siteSocialSettings' => 'Redes Sociales',
    'siteContactsSettings' => 'Contacto',
    'websiteTitle' => 'Titulo del sitio web',
    'metaDescription' => 'Meta Descripción',
    'metaKeywords' => 'Meta Palabras Clave',
    'websiteUrl' => 'URL del Sitio Web',
    'websiteNotificationEmail' => 'Correo electrónico de notificación del sitio web',
    'websiteNotificationEmail1' => 'Envíarme un correo electrónico cuando envien mensajes de contacto',
    'websiteNotificationEmail2' => 'Envíarme un correo electrónico de los nuevos comentarios',
    'websiteNotificationEmail3' => 'Envíarme un correo electrónico de los nuevos pedidos',
    'emailNotifications' => 'Notificaciones de Correo Electronico',
    'contactAddress' => 'Dirección',
    'contactPhone' => 'Telefono',
    'contactFax' => 'Fax',
    'contactMobile' => 'Celular',
    'contactEmail' => 'Correo Electronico',
    'worksTime' => 'Horario de trabajo',
    'siteStatusSettingsPublished' => 'Publicado',
    'siteStatusSettingsClosed' => 'Cerrado',
    'siteStatusSettingsMsg' => 'Cerrado Mensaje',
    'facebook' => 'Facebook',
    'twitter' => 'Twitter',
    'google' => 'Google+',
    'linkedin' => 'Linkedin',
    'youtube' => 'Youtube',
    'instagram' => 'Instagram',
    'pinterest' => 'Pinterest',
    'tumblr' => 'Tumblr',
    'flickr' => 'Flickr',
    'whatapp' => 'Whatsapp',

    'styleSettings' => 'Ajustes de Estilo',
    'siteLogo' => 'Logo del Sitio',
    'favicon' => 'Favicon',
    'appleIcon' => 'Apple Icon',
    'styleColor1' => 'Color 1',
    'styleColor2' => 'Color 2',
    'restoreDefault' => 'Restaurar predeterminado',
    'layoutMode' => 'Modo de diseño',
    'wide' => 'Ampliado',
    'boxed' => 'Boxed',
    'backgroundStyle' => 'Estilo de fondo',
    'colorBackground' => 'Color de fondo',
    'patternsBackground' => 'Patron de fondo',
    'imageBackground' => 'Imagen de fondo',
    'newsletterSubscribe' => 'Suscribirse al Newsletter',
    'footerStyle' => 'Estilo de pie de página',
    'footerStyleBg' => 'Fondo de pie de página',
    'preLoad' => 'Preloader',

    // Banners
    'bannerAdd' => 'Aregar nuevo Banner',
    'bannerEdit' => 'Editar Banner',
    'bannerTitle' => 'Titulo del Banner',
    'bannerSection' => 'Sección',
    'bannerPhoto' => 'Imagen',
    'bannerDetails' => 'Detalles',
    'bannerLinkUrl' => 'URL del Banner',
    'bannerVideoType' => 'Tipo de Video',
    'bannerVideoType1' => 'Subir Video',
    'bannerVideoType2' => 'Youtube',
    'bannerVideoType3' => 'Vimeo',
    'bannerVideoFile' => 'Archivo de video',
    'bannerVideoUrl' => 'Youtube video URL',
    'bannerVideoUrl2' => 'Vimeo video URL',
    'bannerCode' => 'Codigo del Banner',

    // Topics
    'topicNew' => 'Nueva ',
    'topicNewo' => 'Nuevo ',
    'topicEdit' => 'Editar ',
    'topicName' => 'Titulo',
    'topicPhoto' => 'Imagen',
    'topicSection' => 'Sección',
    'topicSelectSection' => 'Seleccionar Sección',
    'topicDate' => 'Fecha',
    'topicAudio' => 'Archivo de Audio',
    'topicVideo' => 'Archivo de Video',
    'topicAttach' => 'Adjuntar archivo',
    'topicDeletedSection' => 'Sin secciones padre',
    'topicTabSection' => 'Detalles de la Sección',
    'topicTabDetails' => 'Detalles del tema',
    'topicSEOTitle' => 'Titulo de la página',
    'topicSEODesc' => 'Meta Descripción',
    'topicSEOKeywords' => 'Meta Palabras Clave',
    'topicAdditionalPhotos' => 'Imagenes',
    'topicGoogleMaps' => 'Google Maps',
    'topicDropFiles' => 'Puedes soltar imagenes aquí o hacer clic para cargar',
    'topicDropFiles2' => 'Puedes cargar varias imagenes al mismo tiempo',
    'topicCommentName' => 'Nombre',
    'topicCommentEmail' => 'Email',
    'topicComment' => 'Comentario',
    'topicNewComment' => 'Nuevo Comentario',
    'topicNewMap' => 'Nuevo Mapa',
    'topicMapTitle' => 'Titulo del Mapa',
    'topicMapDetails' => 'Detalles',
    'topicMapLocation' => 'Localización',
    'topicMapIcon' => 'Icono',
    'topicMapClick' => 'Seleccione la localización donde desea agregar un nuevo marcador',
    'topicMapORClick' => 'O seleccione aqui para agregarlo manualmente',

    // Contacts
    'allContacts' => 'Todos los contactos',
    'waitActivation' => 'Activación de Espera',
    'blockedContacts' => 'Contactos bloqueados',
    'newGroup' => 'Nuevo Grupo',
    'newContacts' => 'Nuevo Contacto',
    'editContacts' => 'Editar Contacto',
    'deleteContacts' => 'Eliminar Contacto',
    'searchAllContacts' => 'Buscar Contacto',
    'firstName' => 'Nombre',
    'lastName' => 'Apellido',
    'companyName' => 'Empresa',
    'city' => 'Ciudad',
    'country' => 'Pais',
    'notes' => 'Notas',
    'group' => 'Grupo',
    'sendEmail' => 'Enviar Email',
    'callNow' => 'Llamar ahora',
    'selectFile' => 'Seleccionar archivo',

    // WebMail
    'labels' => 'Etiquetas',
    'inbox' => 'Bandeja de Entrada',
    'sent' => 'Enviado',
    'draft' => 'Borrador',
    'compose' => 'Nuevo Mensaje',
    'makeAsRead' => 'Marcar como Leido',
    'makeAsUnread' => 'Marcar como No Leido',
    'moveTo' => 'Mover a ',
    'replay' => 'Responder',
    'forward' => 'Reenviar',
    'sendTo' => 'A',
    'sendFrom' => 'De',
    'sendCc' => 'Cc',
    'sendBcc' => 'Bcc',
    'sendTitle' => 'Asunto',
    'SaveToDraft' => 'Guardar en borrador',
    'send' => 'Enviar',
    'print' => 'Imprimir',
    'AttachFiles' => 'Archivos Adjuntos',

    // Calendar
    'newEvent' => 'Nuevo Evento / Nota',
    'eventTotal' => 'Total',
    'eventTitle' => 'Titulo',
    'eventStart' => 'Comienzo del Evento',
    'eventEnd' => 'Fin del Evento',
    'eventStart2' => 'Comienza',
    'eventEnd2' => 'Termina',
    'eventDetails' => 'Detalles',
    'eventNote' => 'Nota',
    'eventMeeting' => 'Reunión',
    'eventEvent' => 'Evento',
    'eventTask' => 'Tarea',
    'eventType' => 'Tipo',
    'eventAt' => 'A las',
    'eventToday' => 'Hoy',
    'eventDay' => 'Dia',
    'eventWeek' => 'Semana',
    'eventMonth' => 'Mes',
    'eventDelete' => '¿Eliminar esta nota/Evento?',
    'eventClear' => '¿Eliminar todas las notas y eventos?',

    // Analytics
    'diagram' => 'Diagrama',
    'barDiagram' => 'Diagrama de barras',
    'visitors' => 'Visitantes',
    'ip' => 'IP',
    'pageViews' => 'Vistas',
    'today' => 'Hoy',
    'yesterday' => 'Ayer',
    'last7Days' => 'Ultima Semana',
    'last30Days' => 'Ultimos 30 dias',
    'thisMonth' => 'Este Mes',
    'lastMonth' => 'Mes Pasado',
    'applyFrom' => 'Desde',
    'applyTo' => 'Hasta',
    'customRange' => 'Rango Personalizado',
    'weekDays' => '"Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"',
    'monthsNames' => '"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"',
    'activity' => 'Actividad',
    'activitiesHistory' => 'Historial de Actividades',

    // Users & permissions
    'newUser' => 'Nuevo Usuario',
    'editUser' => 'Editar Usuario',
    'fullName' => 'Nombre Completo',
    'Permission' => 'Permisos',
    'Permission1' => 'Webmaster',
    'Permission2' => 'Administrador del sitio ( Funciónes completas )',
    'Permission3' => 'Usuario Limitado ( No puede eliminar )',
    'Permission4' => 'Revisar usuario ( Solo puede ver )',
    'personalPhoto' => 'Imagen de Perfil',
    'loginEmail' => 'Email',
    'loginPassword' => 'Contraseña',

    'users' => 'Usuarios',
    'permissions' => 'Permisos',
    'newPermissions' => 'Nuevo Permiso',
    'title' => 'Titulo',
    'dataManagements' => 'Gestión de datos total',
    'dataManagements1' => 'Sus datos solamente (para cuentas personales)',
    'dataManagements2' => 'Datos de todos los usuarios (Para cuentas de administración)',
    'activeApps' => 'Aplicaciones activas',
    'activeSiteSections' => 'Activar Secciones del Sitio',
    'addPermission' => 'AGREGAR Permisos',
    'editPermission' => 'EDITAR Permisos',
    'deletePermission' => 'ELIMINAR Permisos',
    'viewOnly' => 'SÓLO VISTA',
    'editPermissions' => 'Editar Permisos',
    'perAdd' => 'AGREGAR',
    'perEdit' => 'EDITAR',
    'perDelete' => 'ELIMINAR',
    'selectPermissionsType' => 'Seleccionar tipos de permisos',


    // Menus
    'newLink' => 'Nuevo Enlace',
    'newMenu' => 'Nuevo Menu',
    'menuTitle' => 'Titulo del Menu',
    'editSection' => 'Editar Enlace',
    'linkType' => 'Tipo de Enlace',
    'linkType1' => 'Titulo Principal',
    'linkType2' => 'Enlace Directo',
    'linkType3' => 'Sección Principal',
    'linkType4' => 'Lista desplegable',
    'linkURL' => 'URL del Enlace',
    'linkSection' => 'Sección del Enlace',
    'sectionTitle' => 'Enlace/Sección Titulo',
    'fatherSection' => 'Sección Padre',

    // Home
    'latestMessages' => 'Mensajes',
    'notesEvents' => 'Notas y Eventos',
    'latestContacts' => 'Nuevos Contactos',
    'more' => 'Mas',
    'viewMore' => 'Ver Mas',
    'addNew' => 'Agregar Nuevo',
    'reports' => 'Reportes',
    'reportsDetails' => 'Puedes ver mas reportes',
    'hi' => 'Hola',
    'welcomeBack' => 'Bienvenido de vuelta',
    'lastFor7Days' => 'Visitantes de los últimos 7 días',
    'todayByCountry' => 'Hoy por país',
    'browsers' => 'Navegadores',
    'browsersCalculated' => 'Calculado en los últimos 7 días',
    'visitorsRate' => 'Tasa de visitantes',
    'visitorsRateToday' => 'Tasa de visitantes durante el día actual',
    'pages' => 'Páginas',
    'sections' => 'Secciones',
    'resultsFoundFor' => 'Resultados encontrados de',
    'connectEmailToConnect' => 'Conectarse a la cuenta de Webmail',
    'connectEmail' => 'Email',
    'connectPassword' => 'Contraseña',
    'openWebmail' => 'Abrir Webmail',
    'themeSwitcher' => 'Opciones',
    'foldedAside' => 'Comprimir Menu',
    'boxedLayout' => 'Diseño en caja',
    'themes' => 'Temas',
    'themes1' => 'CLARO',
    'themes2' => 'GRIS',
    'themes3' => 'Oscuro',
    'themes4' => 'Negro',
    'language' => 'Idioma',
    'change' => 'Cambiar',
    'sitePreview' => 'Ver Sitio Web',
    'oops' => 'OOPS',
    'noPermission' => "¡Lo siento! No tienes permiso para ver esta página",
    'notFound' => "No pudimos encontrar la página que buscabas",

    // Sign In
    'forgotPassword' => '¿Olvidaste tu contraseña?',
    'signIn' => 'Ingresar',
    'keepMeSignedIn' => 'Manténme conectado',
    'signedInToControl' => 'Iniciar sesión en CONTROL',
    'control' => 'Ciatt',
    'resetPassword' => 'Restablecer la contraseña',
    'confirmPassword' => 'Confirmar Contraseña',
    'newPassword' => 'Nueva Contraseña',
    'yourEmail' => 'Tu Email',
    'sendPasswordResetLink' => 'Enviar enlace para restablecer la contraseña',
    'returnTo' => 'Volver Atras',
    'enterYourEmail' => 'Ingrese su dirección de correo electrónico a continuación y le enviaremos instrucciones sobre cómo cambiar su contraseña.',

];

?>
