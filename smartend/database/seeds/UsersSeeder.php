<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Administrator
        //  - All permissions : add,edit,delete
        //  - All site settings
        //  - All webmaster settings

        $newuser = new User();
        $newuser->name = "Administrador";
        $newuser->email = "admin@admin.com";
        $newuser->password = bcrypt("admin");
        $newuser->permissions_id = "1";
        $newuser->status = "1";
        $newuser->created_by = 1;
        $newuser->save();

        // Site Manager
        //  - All permissions : add,edit,delete
        //  - All site settings

        $newuser = new User();
        $newuser->name = "Gerente";
        $newuser->email = "gerente@gerente.com";
        $newuser->password = bcrypt("gerente");
        $newuser->permissions_id = "2";
        $newuser->status = "1";
        $newuser->created_by = 1;
        $newuser->save();

        // Normal User
        //  - Permissions : add,edit
        //  - No site settings & no delete

        $newuser = new User();
        $newuser->name = "Usuario";
        $newuser->email = "usuario@usuario.com";
        $newuser->password = bcrypt("usuario");
        $newuser->permissions_id = "3";
        $newuser->status = "1";
        $newuser->created_by = 1;
        $newuser->save();

    }
}
